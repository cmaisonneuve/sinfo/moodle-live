Moodle Live
===========

Pour compiler le système, installer `live-build` sur Debian 10 et lancer la
commande:

    sudo lb clean && sudo lb build 2>&1 | tee build.log

Ensuite, transférer les fichiers `vmlinuz`, `initrd.img` et
`filesystem.squashfs` sur le serveur PXE puis créer une entrée de menu.

Exemple d'entrée de menu pour iPXE:

```
:moodlelive
set base-url http://pxe.example.com/bootimg/moodlelive
set base-ipurl http://111.111.111.111/bootimg/moodlelive
set live-args ethdevice-timeout=60 noprompt noeject quickreboot noautologin user-default-groups=video,audio locales=fr_CA.UTF-8
kernel ${base-url}/vmlinuz || start
initrd ${base-url}/initrd.img
imgargs vmlinuz boot=live components quiet splash fetch=${base-ipurl}/filesystem.squashfs ${live-args}
boot
```
